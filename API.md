## Presention

Api CRUD REST pour récupérer, ajouter, supprimer ou modifier des données sur des utilisateurs.

## GET

Get user(s).

**GET all users :**
    url : localhost:8000/api/users

**GET 1 users by id:**
    url : localhost:8000/api/users/id

## POST

**POST with form**

Add an user in database.

url : localhost:8000/api/users

3 key mandatory :
 - firstname
 - name
 - email

id and date_create are automatically generated.

## PUT

**PUT with form**

Modify an user already existing.
With id.
Return error if user not exist or has not been found in database.

url : localhost:8000/api/users/id

3 key mandatory :
 - firstname
 - name
 - email

id and date_create are automatically generated.

NOTE: Please, respect following format of data for PUT Method: {"firstname":"firstname","name":"name","email":"mail@mail.com"}
 - No space
 - No tabulation

## DELETE

**DELETE user with id**

Delete user in database.
Return error if user not exist or has not been found in database.

url : localhost:8000/api/users/id

