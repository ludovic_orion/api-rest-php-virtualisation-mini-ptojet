# API REST php - Virtualisation mini-ptojet

Création d'une API CRUD REST en php à l'aide de docker-compose.

## folder tree

  ./api-project <br/>
      /db <br/>
          /config <br/>
              /schemas <br/>
                  /db.sql <br/>
      /php <br/>
          /src <br/>
              /api <br/>
                  .htacces <br/>
                  db_connect.php <br/>
                  index.php <br/>
                  users.php <br/>
          Dockerfile <br/>
      API.md <br/>
      docker-compose.yml <br/>
      README.MD <br/>

 - **db** contains sql schema which is execute when you build docker-compose.
Used for **data persistence**.

 - **php** contains php files for api REST, .htacces to rewrite url (for API format) and source.

 - **API.md** contains a description of **API CRUD REST**

 - **docker-compose.yml** contains 3 images 
    - apache
    - php
    - mysql
    -phpmyadmin

## Getting started

In root directory :

 - docker-compose build
 - docker-compose up -d

## Requests to test API

request.json provides requests to test API.

## Sources

### Sources for php code for API.
 - Code has been modified. 
 - Architecture has been keeped.
URL=https://waytolearnx.com/ 

