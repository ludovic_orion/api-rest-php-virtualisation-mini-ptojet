<?php
//These are the defined authentication environment in the db service

// The MySQL service named in the docker-compose.yml.
$host = 'db';

// Database use name
$user = 'MYSQL_USER';

//database user password
$pass = 'MYSQL_PASSWORD';

$db = 'MYSQL_DATABASE';

// check the MySQL connection status
$mysqli = new mysqli($host, $user, $pass, $db);
?>