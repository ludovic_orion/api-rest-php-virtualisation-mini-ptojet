<?php
// Connect to database
	include("db_connect.php");
	$request_method = $_SERVER["REQUEST_METHOD"];

	function send_response($response) {
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	function checkUserExist($mysqli, $id){
		$sql = "SELECT id FROM users where id=$id";
		if ($result = $mysqli -> query($sql)) {
			return $result -> num_rows;
		}
	}

	function getUsers($mysqli)
	{
		$sql = "SELECT * FROM users";
		$response = array();
		if ($result = $mysqli -> query($sql)) {
			while ($obj = $result -> fetch_object()) {
				$response[] = $obj;
			}
		}
		return $response;
	}
	
	function getUser($mysqli, $id)
	{
		$sql = "SELECT * FROM users where id=$id";
		$response = array();
		if ($result = $mysqli -> query($sql)) {
			while ($obj = $result -> fetch_object()) {
				$response[] = $obj;
			}
		}
		return $response;
	}
	
	function Addusers($mysqli)
	{
		$firstname = $_POST["firstname"];
		$name = $_POST["name"];
		$email = $_POST["email"];
		$sql="INSERT INTO users(firstname, name, email) VALUES('$firstname', '$name', '$email')";
		$mysqli -> query($sql);
	}
	
	function updateusers($mysqli, $id)
	{
		$firstname = '';
		$name = '';
		$email = '';
		$user = checkUserExist($mysqli, $id);
		if ($user == '0'){
			return 'Error : Data has not been modified - User not found';
		}else {
			parse_str(file_get_contents('php://input'), $_PUT);
			foreach ($_PUT as $key => $value) {
				$r = json_decode($key);
				foreach($r as $k => $v) {
					switch($k) {
						case 'firstname':
							$firstname = $v;
						case 'name':
							$name = $v;
						case 'email':
							$email = $v;
					}
				}
			}
			$sql="UPDATE users SET firstname='".$firstname."', name='".$name."', email='".$email."' WHERE id=".$id;
			$mysqli -> query($sql);	
			return 'Data has been modified';
		}		
	}
	
	function deleteusers($mysqli, $id)
	{
		$user = checkUserExist($mysqli, $id);
		if ($user == '0'){
			return 'Error : Data has not been deleted - User not found';
		}else {
			$sql = "DELETE FROM users where id=$id";
			$mysqli -> query($sql);
			return 'User has been deleted';
		}
	}
	
	switch($request_method)
	{
		
		case 'GET':
			// Retrive users
			if(!empty($_GET["id"]))
			{
				$id=intval($_GET["id"]);
				$response = getUser($mysqli, $id);
				send_response($response);
			}
			else
			{
				$response = getUsers($mysqli);
				send_response($response);
			}
			break;			
		case 'POST':
			// Ajouter un produit
			Addusers($mysqli);
			send_response('Data has been added');
			break;
			
		case 'PUT':
			// Modifier un produit
			$id = intval($_GET["id"]);
			$response = updateusers($mysqli, $id);
			send_response($response);
			break;
			
		case 'DELETE':
			// Supprimer un produit
			$id = intval($_GET["id"]);
			$response = deleteusers($mysqli, $id);
			send_response($response);
			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;
	}
?>